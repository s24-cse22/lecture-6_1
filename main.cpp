#include <iostream>

using namespace std;

int main(){

    // Your code here...

    string theName;
    // cin >> theName;
    getline(cin, theName); // If the user may enter a string with spaces, use getline...

    int age;
    cin >> age;

    float gpa;
    cin >> gpa;


    cout << "Hello, " << theName << "." << endl;
    cout << "It is good to be " << age << " years old." << endl;
    cout << "Your GPA is " << gpa << "." << endl;

    return 0;
}